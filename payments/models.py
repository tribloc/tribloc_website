from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from django.utils import timezone

class Assets(models.Model):
    Shortname = models.CharField(max_length=5,null=True)
    Fullname = models.CharField(max_length=20,null=True)
    Icon = models.FileField(upload_to="assets/", default=None, verbose_name='Asset Icon')
    # AssetTypeId = models.ForeignKey(AssetType,on_delete=models.CASCADE)
    AssetTypeId = models.IntegerField(default=1)
    CreatedAt = models.DateTimeField()
    UpdatedAt = models.DateTimeField()
    UpdatedBy = models.IntegerField()

    def __str__(self):
        return f'Asset {self.pk}'

class AccountBalance(models.Model):
    ClientId = models.ForeignKey(User, on_delete=models.CASCADE)
    AssetId = models.ForeignKey(Assets,on_delete=models.CASCADE)
    Amount = models.DecimalField(decimal_places=8,max_digits=20)
    ReservedAmount = models.DecimalField(decimal_places=8,max_digits=20)
    CreatedAt = models.DateTimeField()
    UpdatedAt = models.DateTimeField()
    UpdatedBy = models.IntegerField()

    def __str__(self):
        return f'Account Balance {self.pk}'

class Wallet(models.Model):
    ClientId = models.ForeignKey(User, on_delete=models.CASCADE)
    AssetId = models.ForeignKey(Assets,on_delete=models.CASCADE)
    WalletTypeId = models.IntegerField()
    WalletStatusId = models.IntegerField()
    WalletDetails = models.TextField(null=True)
    WalletAddress = models.TextField(null=True)
    ApprovalStatusId = models.IntegerField(default=2)
    ReviewedBy = models.IntegerField(null=True)
    ReviewedDate = models.DateTimeField(blank=True, null=True)
    ReviewNotes = models.TextField(null=True)
    CreatedAt = models.DateField(blank=True, null=True)
    UpdatedAt = models.DateField(blank=True, null=True)
    UpdatedBy = models.IntegerField()

    def __str__(self):
        return f'Wallet {self.pk} {self.AssetId.Shortname} {self.WalletAddress}'

class TransferMovement(models.Model):
    ClientId = models.ForeignKey(User, on_delete=models.CASCADE)
    AssetId = models.ForeignKey(Assets,on_delete=models.CASCADE)
    SendWalletId = models.ForeignKey(Wallet, related_name='SendWalletId', on_delete=models.SET_NULL, null=True)
    ReceiveWalletId = models.ForeignKey(Wallet,related_name='ReceiveWalletId', on_delete=models.SET_NULL, null=True)
    # SendWalletId = models.IntegerField(null=True)
    # ReceiveWalletId = models.IntegerField(null=True)
    TransactionDate = models.DateTimeField()
    TransactionTypeId = models.IntegerField()
    TransactionDetails = models.TextField(null=True)
    Amount = models.DecimalField(decimal_places=8,max_digits=20)
    Fee = models.DecimalField(decimal_places=8,max_digits=20)
    ApprovalStatusId = models.IntegerField()
    ReviewedBy = models.IntegerField()
    ReviewedDate = models.DateTimeField()
    CreatedAt = models.DateTimeField()
    UpdatedAt = models.DateTimeField()
    UpdatedBy = models.IntegerField()

    def __str__(self):
        return f'Transfer {self.pk}'