from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path("balances", views.balances, name="balances"),
    path("balances/<int:pk>/", views.transfers, name="transfers"),
    path("landlord_balances", views.landlord_balances, name="landlord_balances"),
    path("landlord_balances/<int:pk>/", views.landlord_transfers, name="landlord_transfers"),
]