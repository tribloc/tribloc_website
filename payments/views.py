from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.core.files.storage import FileSystemStorage
from payments.models import TransferMovement, AccountBalance, Wallet, Assets
from clients.models import Tenant
from django import forms
from datetime import datetime
from django.utils import timezone
import requests
from .services import wallet_check, store_transaction

def get_wallet_address(user, assetId):
    if assetId == 1:
        coin ='tbtc'
        walletId = '5fa50ddcfcdddb001d56e797fddb061e'
    elif assetId == 4:
        coin ='tbch'
        walletId ='5fa7e4aaae38810016279869246570ee'
    elif assetId == 3:
        coin ='teth'
    elif assetId == 2:
        coin ='tltc'
        walletId = '5fa7e56025c1270032f36d8dfc0035e6'

    if Wallet.objects.filter(ClientId=user,AssetId=assetId,WalletTypeId=1).exists():
        WalletReturn = Wallet.objects.filter(ClientId=user,AssetId=assetId,WalletTypeId=1)

    else:
        url = "https://test.bitgo.com/api/v2/%s/wallet/%s/address" % (coin,walletId)
        headers = {"Authorization": "Bearer 0075ba641b9d6eb8aaa04e84d8a2ff995879d78dad026274cc7285ed8af7cf62"}
        label = "ClientId %s Receive Address" % (str(user))
        data = {"label":label} 
        response = requests.request("POST", url, data=data, headers=headers)
        address = response.json()
        Asset = Assets.objects.get(id=assetId)

        wallet = Wallet()
        wallet.ClientId = User.objects.get(id=user)
        wallet.AssetId = Asset
        wallet.WalletTypeId = 1
        wallet.WalletStatusId = 1
        wallet.WalletDetails = address
        wallet.WalletAddress = address['address']
        wallet.ApprovalStatusId = 2
        wallet.ReviewedBy = 1
        wallet.ReviewedDate = timezone.now()
        wallet.ReviewNotes = ''
        wallet.CreatedAt = timezone.now()
        wallet.UpdatedAt = timezone.now()
        wallet.UpdatedBy = 1
        wallet.save()

        WalletReturn = Wallet.objects.filter(ClientId=user,AssetId=assetId,WalletTypeId=1)

    return WalletReturn


@login_required
def balances(request):
    tenant = request.user.tenant.ClientId
    balances = AccountBalance.objects.filter(ClientId=tenant)
    assets = Assets.objects.filter()

    context = {
        'balances': balances,
        'assets': assets
    }
    return render(request, 'payments/tenant_balances.html', context)

@login_required
def transfers(request,pk):
    tenant_user = request.user.tenant.ClientId
    store_transaction(request.user.id,pk)
    transfers = TransferMovement.objects.filter(ClientId=tenant_user,AssetId=pk)
    balances = AccountBalance.objects.filter(ClientId=tenant_user,AssetId_id=pk)
    assets = Assets.objects.filter(id=pk)
    addressToShow = get_wallet_address(request.user.id, pk)

    context = {
        'balances': balances,
        'transfers': transfers,
        'assets': assets,
        'wallets': addressToShow
    }
    return render(request, 'payments/tenant_transfers.html', context)

@login_required
def landlord_balances(request):
    landlord = request.user.landlord.ClientId
    balances = AccountBalance.objects.filter(ClientId=landlord)
    assets = Assets.objects.filter()

    context = {
        'balances': balances,
        'assets': assets
    }
    return render(request, 'payments/landlord_balances.html', context)

@login_required
def landlord_transfers(request,pk):
    landlord = request.user.landlord.ClientId
    transfers = TransferMovement.objects.filter(ClientId=landlord,AssetId=pk)
    balances = AccountBalance.objects.filter(ClientId=landlord,AssetId_id=pk)
    assets = Assets.objects.filter(id=pk)
    addressToShow = get_wallet_address(request.user.id, pk)

    context = {
        'balances': balances,
        'transfers': transfers,
        'assets': assets,
        'wallets': addressToShow
    }
    return render(request, 'payments/landlord_transfers.html', context)