import datetime as dt
from datetime import datetime
from django.utils import timezone
from payments.models import TransferMovement, AccountBalance, Wallet, Assets
from django.contrib.auth.models import User
import requests
import json



url = 'https://test.bitgo.com/api/v2'
headers = {'Authorization': 'Bearer 0075ba641b9d6eb8aaa04e84d8a2ff995879d78dad026274cc7285ed8af7cf62'}

def get_bitgo_transactions(assetId):
    if assetId == 1:
        coin ='tbtc'
        walletId = '5fa50ddcfcdddb001d56e797fddb061e'
    elif assetId == 4:
        coin ='tbch'
        walletId ='5fa7e4aaae38810016279869246570ee'
    elif assetId == 3:
        coin ='teth'
    elif assetId == 2:
        coin ='tltc'
        walletId = '5fa7e56025c1270032f36d8dfc0035e6'

    date = datetime.utcnow().date()
    transactions = []

    try:
        last_db_datetime = TransferMovement.objects.filter(AssetId=assetId).latest('CreatedAt')
        print(last_db_datetime.CreatedAt)
        lastTime = str(last_db_datetime.CreatedAt + dt.timedelta(seconds=1))
        params = {'dateGte': lastTime, 'coin':coin}
    except:
        params = {'coin':coin}

    response = requests.request("GET", url+'/'+coin+'/wallet/'+walletId+'/transfer',params=params, headers=headers)
    tranResponse = response.json()

    transactions.append(tranResponse)
    return transactions

def wallet_check(recaddr,assetId):
  print(recaddr)
  transactions = get_bitgo_transactions(assetId)
  transactions = transactions[0]
  sendAddress = 'NA'

  for key in transactions['transfers']:
    if key['type'] == 'receive':
      for output in key['entries']:
        if key['type'] == 'receive' and output['value'] < 0:
            sendAddress = output['address']
        elif key['type'] == 'send' and 'wallet' not in output and output['value'] < 0:
            sendAddress = output['address']

        walletClient = Wallet.objects.get(WalletAddress=recaddr)
        if Wallet.objects.filter(WalletDetails__contains=sendAddress).exists():
            continue
        else:
            Asset = Assets.objects.get(id=assetId)
            wallet = Wallet()
            wallet.ClientId = User.objects.get(id=walletClient.ClientId.id)
            wallet.AssetId = Asset
            wallet.WalletTypeId = 2
            wallet.WalletStatusId = 1
            wallet.WalletDetails = sendAddress
            wallet.WalletAddress = sendAddress
            wallet.ApprovalStatusId = 2
            wallet.ReviewedBy = 1
            wallet.ReviewedDate = timezone.now()
            wallet.ReviewNotes = ''
            wallet.CreatedAt = timezone.now()
            wallet.UpdatedAt = timezone.now()
            wallet.UpdatedBy = 1
            wallet.save()

  return sendAddress


def store_transaction(user,assetId):
  transactions = []
  transactions = get_bitgo_transactions(assetId)
  print(transactions)
  print(len(transactions))

  i = 0

  while i < len(transactions):
    for key in transactions[i]['transfers']:
      if key['type'] == 'receive':
        print("In")
        coin = (key['coin'][1:]).upper()
        sendAddress = 'NA'
        recieveAddress = 'NA'

        # Not sure if there is a good way to get the legitimate send address (breaks into several addresses)
        # works well when I use bitpay, internal transactions with BitGo seem different
        for output in key['entries']:
            if key['type'] == 'receive' and 'wallet' in output and output['value'] > 0:
                recieveAddress = output['address']
            elif(key['type'] == 'send' and 'wallet' not in output and output['value'] > 0):
                recieveAddress = output['address']
            if key['type'] == 'receive' and output['value'] < 0:
                sendAddress = output['address']
            elif key['type'] == 'send' and 'wallet' not in output and output['value'] < 0:
                sendAddress = output['address']

        confirmDate = key['date'][:10]
        confirmTime = key['date'].split('T')[1][:-1]
        transactionDate = confirmDate + ' ' + confirmTime
        transactionDate = datetime.strptime(transactionDate, '%Y-%m-%d %H:%M:%S.%f')

        amount = abs((key['baseValue']) / 100000000)
        if key['type'] == 'receive':
            tranType = 1
            fee = 0
        elif key['type'] == 'send':
            tranType = 2
            fee = ((float(key['feeString'])))*-1 / 100000000
        
        txid = key['txid']
        tranDetails = json.dumps(key)
        wallet_check(recieveAddress,assetId)

        recWallet = Wallet.objects.get(WalletAddress=recieveAddress)
        print(recWallet.WalletAddress)
        recWallId = recWallet.WalletAddress


        sendWallet = Wallet.objects.get(WalletAddress=sendAddress)
        print(sendWallet.WalletAddress)
        sendWallId = sendWallet.WalletAddress

        print('recWalletId : '+str(recWallId))
        print('recWalletAddress : '+str(recieveAddress))
        print('sendWalletId : '+str(sendWallId))
        print('sendWalletAddress : '+str(sendAddress))

        print('-----------------------NEW TRANSACTION--------------------')
        transfer = TransferMovement()
        transfer.ClientId = User.objects.get(id=recWallet.ClientId.id)
        Asset = Assets.objects.get(id=recWallet.AssetId.id)
        transfer.AssetId = Asset
        transfer.SendWalletId = sendWallet
        transfer.ReceiveWalletId = recWallet
        transfer.TransactionDate = timezone.now()
        transfer.TransactionTypeId = 1
        transfer.TransactionDetails = tranDetails
        transfer.Amount = amount
        transfer.Fee = fee
        transfer.ApprovalStatusId = 2
        transfer.ReviewedBy = 1
        transfer.ReviewedDate = timezone.now()
        transfer.CreatedAt = timezone.now()
        transfer.UpdatedAt = timezone.now()
        transfer.UpdatedBy = 1
        transfer.save()

    i = i + 1