from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path("", views.homepage, name="homepage"),
    path("properties", views.project_index, name="project_index"),
    path("<int:pk>/", views.project_detail, name="project_detail"),
    path("create_listing", views.create_listing, name="create_listing"),
    path("create_listing_save", views.create_listing_save, name="create_listing_save"),
    path("landlord_properties", views.landlord_properties, name="landlord_properties"),
    path("landlord_properties_tableview", views.landlord_properties_tableview, name="landlord_properties_tableview"),
    path("landlord_property_applications/<int:pk>/", views.landlord_property_applications, name="landlord_property_applications"),
    path("tenant_applications/<int:pk>/", views.tenant_applications, name="tenant_applications"),

] 
