from django.db import models
from PIL import Image
from django.contrib.auth.models import User
# from django.contrib.auth import get_user_model
# from django.conf import settings
from clients.models import Landlord, Tenant
from properties.choices import *
# import datetime
from django.utils.translation import gettext_lazy as _

class Properties(models.Model):
    OwnerClientId = models.ForeignKey(User, on_delete=models.CASCADE)
    AddressLine1 = models.CharField(max_length=30,blank=True,null=True)
    AddressLine2 = models.CharField(max_length=30,blank=True,null=True)
    County = models.CharField(blank=True,null=True,choices=COUNTY_CHOICES,max_length=200)
    City = models.CharField(max_length=10,blank=True,null=True)
    Country = models.CharField(max_length=30,blank=True,null=True)
    Price = models.IntegerField(verbose_name='Cost of rent per month')
    DepositAmount = models.IntegerField(blank=True,null=True,verbose_name='Deposit Amount')
    Description = models.TextField(blank=True,null=True)
    PropertyType = models.CharField(choices=TYPE_CHOICES,max_length=200,default='House',verbose_name='Type of property')
    NoOfBedrooms = models.IntegerField(choices=NUMBER_CHOICES,default=1,verbose_name='No. Bedrooms')
    NoOfBathrooms = models.IntegerField(choices=NUMBER_CHOICES,default=1,verbose_name='No. Bathrooms')
    SalaryRequirement = models.IntegerField(null=True,verbose_name='Tenant expected salary')
    ReferenceRequired = models.IntegerField(choices=YESNO_CHOICES,default=0,null=True,verbose_name='Reference required?')
    PreviewImage = models.FileField(upload_to="house_preview/", default=None, verbose_name='Select an image that describes this property best')
    ListingStatus = models.CharField(choices=Listing_CHOICES,default='Active',max_length=14, verbose_name='Do you edit your listing?')
    CreatedAt = models.DateTimeField(null=True)
    UpdatedAt = models.DateTimeField(null=True)
    UpdatedBy = models.IntegerField(default=1)

    def __str__(self):
        return f'{self.pk} Property'

class PropertyApplications(models.Model):
    ApplicantClientId = models.ForeignKey(Tenant,to_field='ClientId', on_delete=models.CASCADE)
    OwnerClientId = models.ForeignKey(Landlord,to_field='ClientId', on_delete=models.CASCADE)
    PropertyId = models.ForeignKey('Properties', on_delete=models.CASCADE)
    Note = models.TextField(_('Send Message to the Landlord'),default='Would you like to write a message with your application?')
    ApplicationStatus = models.CharField(choices=Application_CHOICES,default='Pending',max_length=30)
    CreatedAt = models.DateTimeField(null=True)
    UpdatedAt = models.DateTimeField(null=True)
    UpdatedBy = models.IntegerField(default=1)

    def __str__(self):
        return f'{self.pk} Application'

# gets the directory name for property images
def get_property_image_filenames(instance, filename):
    AddressLine1 = instance.PropertyId.AddressLine1
    return "house_preview/%s/image" % (AddressLine1)

class PropertyImages(models.Model):
    PropertyId = models.ForeignKey(Properties, on_delete=models.CASCADE)
    PropertyImages = models.FileField(upload_to=get_property_image_filenames, default=None,verbose_name='Select property images')

class PropertyLease(models.Model):
    TenantClientId = models.ForeignKey(Tenant,to_field='ClientId', on_delete=models.CASCADE)
    OwnerClientId = models.ForeignKey(Landlord,to_field='ClientId', on_delete=models.CASCADE)
    PropertyId = models.ForeignKey('Properties', on_delete=models.CASCADE)
    LeaseStatus = models.CharField(default='Active',max_length=30)
    LeaseStartDate = models.DateTimeField(null=True)
    LeaseEndDate = models.DateTimeField(null=True)
    CreatedAt = models.DateTimeField(null=True)
    UpdatedAt = models.DateTimeField(null=True)
    UpdatedBy = models.IntegerField(default=1)

    def __str__(self):
        return f'{self.pk} PropertyLease'