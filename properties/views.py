from django.shortcuts import render, redirect
from django.urls import reverse
from properties.models import Properties, PropertyApplications, PropertyImages
from clients.models import Tenant, Landlord
from .filters import Filter
from .forms import CreatingListingForm, ListingApplicationForm, ManageListingForm,ImageForm, PropertyLeaseForm
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.utils import timezone

# Create your views here.
def project_index(request):
    # displays all properties available on the site
    properties = Properties.objects.filter(ListingStatus='Active')
    # myFilter allows to filter properties based on location, bedrooms, bathrooms and type of property
    myFilter = Filter(request.GET, queryset=properties)
    properties = myFilter.qs
    context = {
        'properties': properties, 
        'myFilter': myFilter
    }
    return render(request, 'properties/project_index.html', context)

def project_detail(request,pk,*args,**kwargs):
    properties = Properties.objects.get(pk=pk)
    apply_button = ListingApplicationForm(request.POST)
    landlord = Landlord.objects.get(ClientId=properties.OwnerClientId)
    property_images = PropertyImages.objects.filter(PropertyId=properties)
    context = {
        'properties': properties,
        'property_images' : property_images,
        'application_form' : apply_button
    }
    if request.user.is_authenticated:
        if request.user.last_name == 'False': # allows to tenant to view ads and apply for them if they meet the requirements
            tenant = Tenant.objects.get(ClientId=request.user.tenant.ClientId)
            if PropertyApplications.objects.filter(ApplicantClientId=tenant,PropertyId=properties).exists():
                applications = PropertyApplications.objects.get(ApplicantClientId=tenant,PropertyId=properties)
            else:
                applications = apply_button
            context['property_applications'] = applications
            context['tenant'] = tenant
 
            if request.method == "POST":
                link = apply_button.save(commit=False)
                link.ApplicantClientId = tenant
                link.OwnerClientId = landlord
                link.PropertyId = properties
                link.ApplicationStatus = 'Pending'
                link.CreatedAt = timezone.now()
                link.UpdatedAt = timezone.now()
                link.UpdatedBy = 1
                link.save()
                messages.success(request, f'You have applied!')
                return redirect("/properties")
            else:
                link = apply_button
                messages.info(request, 'When you apply you agree to sharing your profile information with the property owner.')

    return render(request, 'properties/project_detail.html', context)


@login_required
def create_listing(request):
    listing_form = CreatingListingForm()
    image_form = ImageForm()

    return render(request, 'properties/create_listing.html', {'listing_form': listing_form,'image_form': image_form})


def create_listing_save(request):
    if request.method!="POST":
        return HttpResponseRedirect(reverse("create_listing"))
    else:
        listing_form = CreatingListingForm(request.POST, request.FILES)
        images = request.FILES.getlist('PropertyImages')
        listing = listing_form.save(commit=False)
        listing.OwnerClientId = request.user
        listing.Country = 'Ireland'
        listing.ListingStatus = 'Active'
        listing.CreatedAt = timezone.now()
        listing.UpdatedAt = timezone.now()
        listing.UpdatedBy = 1
        listing.save()
        for i in images:
            image_instance = PropertyImages(PropertyImages=i, PropertyId=listing)
            image_instance.save()
        messages.success(request, f'Your listing has been created!!')
            # return redirect('/portal/', args=link.pk)
        return HttpResponseRedirect(reverse('create_listing'))

def homepage(request):
    return render(request, 'properties/homepage.html')

@login_required
def landlord_properties(request):
    landlord_user = request.user.landlord.ClientId
    properties = Properties.objects.filter(OwnerClientId=landlord_user)
    context = {
        'properties': properties
    }

    return render(request, 'properties/landlord_properties.html', context)

@login_required
def landlord_properties_tableview(request):
    landlord_user = request.user.landlord.ClientId
    properties = Properties.objects.filter(OwnerClientId=landlord_user)
    context = {
        'properties': properties
    }

    return render(request, 'properties/landlord_properties_tableview.html', context)

@login_required
def landlord_property_applications(request,pk):
    lease_form = PropertyLeaseForm(request.POST)
    property_applications = PropertyApplications.objects.filter(PropertyId=pk)
    properties = Properties.objects.get(id=pk)
    context = {
        'properties': properties,
        'property_applications': property_applications,
        'lease_form': lease_form
    }

    return render(request, 'properties/property_applications.html', context)

@login_required
def tenant_applications(request,pk):
    property_applications = PropertyApplications.objects.filter(ApplicantClientId=pk)
    context = {
        'property_applications':property_applications
    }

    return render(request, 'properties/tenant_applications.html', context)