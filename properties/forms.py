from django import forms
from django.forms import ModelForm
from django.contrib.auth.models import User
from django.forms import ClearableFileInput
from .models import Properties, PropertyApplications, PropertyImages, PropertyLease

class CreatingListingForm(forms.ModelForm):
    class Meta:
        model = Properties
        fields = [
            'PreviewImage',
            'AddressLine1',
            'AddressLine2',
            'City',
            'County',
            'PropertyType',
            'Description',
            'NoOfBedrooms',
            'NoOfBathrooms',
            'Price',
            'DepositAmount',
            'SalaryRequirement',
            'ReferenceRequired'
        ]
        widgets = {
            'AddressLine1': forms.TextInput(attrs={'placeholder': 'Address Line 1'}),
            'AddressLine2': forms.TextInput(attrs={'placeholder': 'Address Line 2'}),
            'City': forms.TextInput(attrs={'placeholder': 'City / Town'}),
            'Description': forms.Textarea(attrs={'placeholder': 'Description'}),
        }
#
class ManageListingForm(forms.ModelForm):
    class Meta:
        model = Properties
        fields = [
            'Description',
            'Price',
            'SalaryRequirement',
            'ReferenceRequired',
            'PreviewImage',
            'ListingStatus'
        ]

# form that allows to upload multiple images files
class ImageForm(forms.ModelForm):
    class Meta:
        model = PropertyImages
        fields = ['PropertyImages']
        widgets = {'PropertyImages': ClearableFileInput(attrs={'multiple': True}),}
        # widget is important to upload multiple files

class ListingApplicationForm(forms.ModelForm):
    class Meta:
        model = PropertyApplications
        fields = ['Note']

class PropertyLeaseForm(forms.ModelForm):
    class Meta:
        model = PropertyLease
        fields = ['LeaseStartDate','LeaseEndDate']