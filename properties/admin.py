from django.contrib import admin
from .models import Properties, PropertyApplications, PropertyImages

admin.site.register(Properties)
admin.site.register(PropertyApplications)
admin.site.register(PropertyImages)