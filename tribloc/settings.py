import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '8x+p778ii32g4*h7)*)x95sbo^gk$w31_xbh_2bu_x%5(4a*h-'
BITGO_BEARER = "Bearer 0075ba641b9d6eb8aaa04e84d8a2ff995879d78dad026274cc7285ed8af7cf62"
# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']

# Application definition

INSTALLED_APPS = [
    'properties',
    'payments',
    'clients.apps.ClientsConfig',
    'crispy_forms',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_filters',
    'storages',
    'rest_framework',
    'qr_code',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'tribloc.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': ["tribloc/templates/"],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.media'
            ],
        },
    },
]

WSGI_APPLICATION = 'tribloc.wsgi.application'

#SQLITE3 config
# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.sqlite3',
#         'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
#     }
# }

# #PostgreSQL config
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'tribloc',
        'HOST': "pgdb-tribloc-production-northeu-01.postgres.database.azure.com",
        'USER': 'triblocadmin@pgdb-tribloc-production-northeu-01',
        'PASSWORD': 'tr!bl0c2012'
    }
}

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/3.0/topics/i18n/
LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.0/howto/static-files/
STATIC_URL = '/static/'

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'static')
]
# Full path to dorectory where django stores media file
# Stored on FileSystem and not DB for performance reasons
MEDIA_ROOT = os.path.join(BASE_DIR, 'media/')
# How we access media through the web browser
MEDIA_URL = 'media/'
CRISPY_TEMPLATE_PACK = 'bootstrap4'
DEFAULT_FILE_STORAGE = 'tribloc.static_azure.AzureMediaStorage'
STATICFILES_STORAGE = 'tribloc.static_azure.AzureStaticStorage'
STATIC_LOCATION = "static/"
MEDIA_LOCATION = "media/"

AZURE_ACCOUNT_NAME = "storagetbstgnne1"
AZURE_CUSTOM_DOMAIN = f'storagetbstgnne1.blob.core.windows.net/'
STATIC_URL = f'https://storagetbstgnne1.blob.core.windows.net/static/'
MEDIA_URL = f'https://storagetbstgnne1.blob.core.windows.net/media/'



# Redirects to Home page after successful login 
LOGIN_REDIRECT_URL = '/'
#Takes you to login page if you try to access a page that requires a user to be sign in
LOGIN_URL = 'login'


# Sendgrid Details for Emails
EMAIL_HOST = 'smtp.sendgrid.net'
EMAIL_HOST_USER = 'apikey' # this is exactly the value 'apikey'
EMAIL_HOST_PASSWORD = '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'
EMAIL_PORT = 587
EMAIL_USE_TLS = True

