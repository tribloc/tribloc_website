from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path, include
from clients import views as user_views
from payments import views as payment_views
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    path('admin/', admin.site.urls),
    path("", include("properties.urls"), name='properties'),
    path("register", user_views.register, name ='register'),
    path("profile/", user_views.profile, name ='profile'),
    path("login/", auth_views.LoginView.as_view(template_name='clients/login.html'), name ='login'),
    path("logout/", auth_views.LogoutView.as_view(template_name='clients/logout.html'), name ='logout'),
    path("portal/", user_views.portal, name ='portal'),
    path("landlord_portal/", user_views.landlord_portal, name ='landlord_portal'),
    path("balances/", payment_views.balances, name ='balances'),
    path("landlord_balances/", payment_views.landlord_balances, name ='landlord_balances'),
    path("tenant_public_profile/<int:pk>/", user_views.tenant_public_profile, name ='tenant_public_profile'),
    path("", include("payments.urls"), name='payments'),
] 

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
