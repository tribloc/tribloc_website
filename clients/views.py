from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.core.files.storage import FileSystemStorage
from .forms import UserRegistrationForm, UserUpdateForm, TenantProfileUpdateForm, LandlordProfileUpdateForm
from .models import Tenant, Landlord
from django import forms
import datetime

def register(request):
    # allows users to register
    # UserRegistrationForm is used to collect data, like username, password etc
    if request.method == 'POST':
        form = UserRegistrationForm(request.POST)
        if form.is_valid():
            # username = form.cleaned_data.get('email')
            email = form.cleaned_data.get('email')
            last_name = form.cleaned_data.get('last_name')
            form.save()
            messages.success(request, f'Your account has been created! You are now able to log in')
            return redirect('login')
    else:
        form = UserRegistrationForm()
    return render(request, 'clients/register.html', {'form': form})


@login_required
def profile(request, *args, **kwargs):
    if request.method == 'POST':
        if request.user.last_name == 'False':
            p_form = TenantProfileUpdateForm(request.POST, request.FILES, instance=request.user.tenant)
        else:
            p_form = LandlordProfileUpdateForm(request.POST, request.FILES, instance=request.user.landlord)
        if p_form.is_valid():
            p_form = p_form.save(commit=False)
            p_form.KycStatusId = 2
            p_form.save()
            messages.success(request, f'Your account has been updated!')
            if request.user.last_name == 'False':
                return redirect('portal')
            else:
                if p_form.is_valid():
                    p_form.save()
                    return redirect('portal')
    else:
        if request.user.last_name == 'False':
            p_form = TenantProfileUpdateForm(instance=request.user.tenant)
            messages.info(request, f'Click "Update" to store your details')
        else:
            p_form = LandlordProfileUpdateForm(instance=request.user.landlord)
            messages.info(request, f'Click "Update" to store your details')
    context = {'p_form': p_form, }
    return render(request, 'clients/profile.html', context)

@login_required
def portal(request):
    # based on usertype, displays Tenant or Landlord portal
    return render(request, 'clients/tenant_portal.html')

@login_required
def landlord_portal(request):
    # based on usertype, displays Tenant or Landlord portal
    return render(request, 'clients/landlord_portal.html')

@login_required
def tenant_public_profile(request,pk):
    tenant = Tenant.objects.get(ClientId=pk)
    context = {
        'tenant' : tenant
    }
    return render(request, 'clients/tenant_public_profile.html', context)

