from django.contrib import admin
from .models import Tenant, Landlord, ClientReviews

admin.site.register(Tenant)
admin.site.register(Landlord)
admin.site.register(ClientReviews)