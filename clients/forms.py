from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm 
from .models import Tenant, Landlord
from django.forms import ModelForm, Textarea
from .models import usertypechoices
from .choices import *
from django.utils.translation import ugettext_lazy as _

class UserRegistrationForm(UserCreationForm):
    last_name = forms.ChoiceField(
        choices=usertypechoices,
        label='You Are A'
    )
    class Meta:
        model = User
        fields = ['last_name','username','email','password1', 'password2']

class UserUpdateForm(forms.ModelForm):
    email = forms.EmailField()
    class Meta:
        model = User
        fields = ['username', 'email',]

class DateInput(forms.DateInput):
    input_type = 'date'

class TenantProfileUpdateForm(forms.ModelForm):
    Gender = forms.ChoiceField(choices=GENDER_CHOICES)
    EmploymentStatusId = forms.ChoiceField(choices=EMPLOYMENTSTATUS_CHOICES)
    EmployerTypeId = forms.ChoiceField(choices=EMPLOYERTYPE_CHOICES)
    EmployerIndustryId = forms.ChoiceField(choices=INDUSTRY_CHOICES)
    class Meta:
        model = Tenant
        fields = ['Firstname','Middlename','Lastname','DateOfBirth','Gender',
            'NationalityCountryId','Telephone','Identification','Selfie','Reference',
            'Salary','SalaryDocument','Hap','HapDocument',
            "EmploymentStatusId","EmployerTypeId","EmployerName","EmployerIndustryId",
            "EmploymentDocument","PreviousLandlordName","PreviousLandlordEmail",
            "PreviousLandlordPhone","Savings",'BankStatement']
        widgets = {
            'Firstname': forms.TextInput(attrs={'placeholder': 'First Name','class':'my_class'}),
            'Middlename': forms.TextInput(attrs={'placeholder': 'Middle Name','class':'my_class'}),
            'Lastname': forms.TextInput(attrs={'placeholder': 'Last Name','class':'my_class'}),
            'Identification': forms.FileInput(attrs={'placeholder': 'ID','class':'imageUpload'}),
            'Selfie': forms.FileInput(attrs={'placeholder': 'Selfie','class':'imageUpload'}),
            'DateOfBirth': DateInput(attrs={'placeholder': 'DOB','class':'my_class'}),
            'Gender': forms.TextInput(attrs={'class':'my_class'}),
            'NationalityCountryId': forms.TextInput(attrs={'placeholder': 'Nationality','class':'my_class'}),
            'Telephone': forms.TextInput(attrs={'placeholder': 'Phone Number','class':'my_class'}),
            'EmploymentStatusId': forms.TextInput(attrs={'placeholder': 'Employment Status','class':'my_class'}),
            'EmployerTypeId': forms.TextInput(attrs={'placeholder': 'Employment Type','class':'my_class'}),
            'EmployerName': forms.TextInput(attrs={'placeholder': 'Employer Name','class':'my_class'}),
            'EmployerIndustryId': forms.TextInput(attrs={'placeholder': 'Employment Industry','class':'my_class'}),
            'Salary': forms.NumberInput(attrs={'placeholder': 'Salary','class':'my_class'}),
            'EmploymentDocument': forms.FileInput(attrs={'placeholder': 'Employment Letter','class':'documentUpload'}),
            'PreviousLandlordName': forms.TextInput(attrs={'placeholder': 'Previous Landlord','class':'my_class'}),
            'PreviousLandlordEmail': forms.TextInput(attrs={'placeholder': 'Previous Landlord Email','class':'my_class'}),
            'PreviousLandlordPhone': forms.TextInput(attrs={'placeholder': 'Previous Landlord Phone','class':'my_class'}),
            'Reference': forms.FileInput(attrs={'placeholder': 'Landlord Reference','class':'documentUpload'}),
            'Savings': forms.NumberInput(attrs={'placeholder': 'Cash at Bank','class':'my_class'}),
            'BankStatement': forms.FileInput(attrs={'placeholder': 'Bank Statement','class':'documentUpload'}),
        }

class LandlordProfileUpdateForm(forms.ModelForm):
    class Meta:
        model = Landlord
        fields = ['Identification','Selfie']






