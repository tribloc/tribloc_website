from django.db import models
from django.contrib.auth.models import User
from PIL import Image
from clients.choices import *
usertypechoices = [(True,'Landlord'),(False,'Tenant')]

# Create your models here.
class Tenant(models.Model):
    ClientId = models.OneToOneField(User, on_delete=models.CASCADE, unique=True)
    KycStatusId = models.IntegerField(default=1)
    Firstname = models.CharField(blank=True,null=True,max_length=30)
    Middlename = models.CharField(blank=True,null=True,max_length=30)
    Lastname = models.CharField(blank=True,null=True,max_length=30)
    Gender = models.CharField(blank=True,null=True,max_length=30,choices=GENDER_CHOICES)
    DateOfBirth = models.DateField(blank=True,null=True)
    NationalityCountryId = models.IntegerField(blank=True,null=True)
    Telephone = models.CharField(blank=True,null=True,max_length=30)
    ApprovalStatusId = models.IntegerField(default=1)
    ReviewedBy = models.IntegerField(blank=True,null=True)
    ReviewedDate = models.DateTimeField(blank=True,null=True)
    EmploymentStatusId = models.IntegerField(blank=True,null=True)
    EmployerTypeId = models.IntegerField(blank=True,null=True)
    EmployerName = models.CharField(blank=True,null=True,max_length=50)
    EmployerIndustryId = models.IntegerField(blank=True,null=True)
    EmploymentDocument = models.FileField(upload_to='docs',null=True,blank=True)
    PreviousLandlordName = models.CharField(blank=True,null=True,max_length=30)
    PreviousLandlordEmail = models.CharField(blank=True,null=True,max_length=30)
    PreviousLandlordPhone = models.CharField(blank=True,null=True,max_length=30)
    Savings = models.FloatField(blank=True,null=True)
    Identification = models.FileField(upload_to='docs',null=True,blank=True)
    Selfie = models.ImageField(default='default.jpg', upload_to='profile_pics')
    Salary = models.FloatField(blank=True,null=True)
    SalaryDocument = models.FileField(upload_to='docs',null=True,blank=True)
    Reference = models.FileField(upload_to='docs',null=True, blank=True)
    BankStatement = models.FileField(upload_to='docs',null=True,blank=True)
    Hap = models.BooleanField(default=False)
    HapDocument = models.FileField(upload_to='docs',null=True, blank=True)
    CreatedAt = models.DateTimeField(blank=True,null=True)
    UpdatedAt = models.DateTimeField(blank=True,null=True)
    UpdatedBy = models.IntegerField(default=1)

    def __str__(self):
        return f'{self.pk} Tenant Profile'

class Landlord(models.Model):
    ClientId = models.OneToOneField(User, on_delete=models.CASCADE)
    KycStatusId = models.IntegerField(default=1)
    Firstname = models.CharField(blank=True,null=True,max_length=30)
    Middlename = models.CharField(blank=True,null=True,max_length=30)
    Lastname = models.CharField(blank=True,null=True,max_length=30)
    Gender = models.CharField(blank=True,null=True,max_length=30)
    DateOfBirth = models.DateField(blank=True,null=True)
    NationalityCountryId = models.IntegerField(blank=True,null=True)
    Telephone = models.CharField(blank=True,null=True,max_length=30)
    ApprovalStatusId = models.IntegerField(default=1)
    ReviewedBy = models.IntegerField(blank=True,null=True)
    ReviewedDate = models.DateTimeField(null=True,blank=True)
    Identification = models.FileField(upload_to='docs',null=True,blank=True)
    Selfie = models.ImageField(default='default.jpg', upload_to='profile_pics')
    CreatedAt = models.DateTimeField(blank=True,null=True)
    UpdatedAt = models.DateTimeField(blank=True,null=True)
    UpdatedBy = models.IntegerField(default=1)

    def __str__(self):
        return f'{self.pk} Landlord Profile'

class ClientReviews(models.Model):
    TenantClientId = models.ForeignKey(Tenant, on_delete=models.CASCADE)
    LandlordClientId = models.ForeignKey(Landlord, on_delete=models.CASCADE)
    ReviewType = models.IntegerField(default=1)
    ReviewDetails = models.TextField()
    Stars = models.IntegerField(default=5)
    CreatedAt = models.DateTimeField(blank=True,null=True)
    UpdatedAt = models.DateTimeField(blank=True,null=True)
    UpdatedBy = models.IntegerField(default=1)

    def __str__(self):
        return f'{self.pk} Review'